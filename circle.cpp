#include "circle.h"
Ellipse::Ellipse(int radius, int radius2){
	R1 = radius;
	R2 = radius2;
}

Ellipse::Ellipse(int radius){
	R1 = R2 = radius;
}

double Ellipse::area(){
	return R1 * R2 * pi;
}

double Ellipse::perimeter(){
	return pi * (R1 + R2);
}

void Ellipse::name(){
	if (R1 != R2)
		cout<<"Ellipse\n";
	else
		cout<<"Circle\n";
}
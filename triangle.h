#ifndef _TRIANGLE_H
#define _TRIANGLE_H
#include "figures.h"
class Triangle: public Figure{
public:
	int a, b, c;
	Triangle(int sidea, int sideb, int sidec);
	double area();
	double perimeter();
	void name();
};
#endif
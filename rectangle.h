#ifndef _RECTANGLE_H
#define _RECTANGLE_H
#include "figures.h"
class Rectangle: public Figure{
public:
	int a, b;
	Rectangle(int sidea, int sideb);
	Rectangle(int side);
	double area();
	double perimeter();
	void name();
};
#endif
#ifndef _FIGURES_H
#define _FIGURES_H

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

const double pi = 3.14159265358979323846;

struct Point{
	int x;
	int y;
};

class Figure{
public:
	virtual double area() = 0;
	virtual double perimeter() = 0;
	virtual void name() = 0;
};

/**/

#endif
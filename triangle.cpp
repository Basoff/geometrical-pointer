#include "triangle.h"
Triangle::Triangle(int sidea, int sideb, int sidec){
	a = sidea; b = sideb; c = sidec;
}

double Triangle::area(){
	double p = (a + b + c)/2;
	return sqrt(p * (p-a) * (p-b) * (p-c));
}

double Triangle::perimeter(){
	return a + b + c;
}

void Triangle::name(){
	cout<<"Triangle\n";
}

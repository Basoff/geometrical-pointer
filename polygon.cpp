#include "polygon.h"
Polygon::Polygon(){
	point_num = 0;
}

void Polygon::addPoint(int x, int y){
	Point adding;
	adding.x = x;
	adding.y = y;
	points.push_back(adding);
	point_num ++;
}

double Polygon::area(){
	int area = 0;
	int help = 0;
	for (int i = 0; i < point_num; i++)
	{
		area += points[i].x * points[(i+1 % (point_num + 1))].y;
		help += points[i].y * points[(i+1 % (point_num + 1))].x;
	}
	if (area - help > 0)
		return (area - help)/2;
	else
		return (help - area)/2;
}

double Polygon::perimeter(){
	double length;
	for (int i = 0; i < point_num; i++)
	{
		length += sqrt((points[i].x - points[(i+1 % (point_num + 1))].x)*(points[i].x - points[(i+1 % (point_num + 1))].x) + 
		(points[i].y - points[(i+1 % (point_num + 1))].y)*(points[i].y - points[(i+1 % (point_num + 1))].y));
	}
	return length;
}

void Polygon::name(){
	cout<<"Polygon\n";
}
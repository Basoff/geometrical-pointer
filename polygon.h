#ifndef _POLYGON_H
#define _POLYGON_H
#include "figures.h"
class Polygon: public Figure{
public:
	vector <Point> points;
	int point_num;
	Polygon();
	void addPoint (int x, int y);
	double area();
	double perimeter();
	void name();
};

#endif
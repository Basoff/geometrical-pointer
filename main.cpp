#include <iostream>
using namespace std;
#include "figures.h"
#include "circle.h"
#include "triangle.h"
#include "rectangle.h"
#include "polygon.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	Figure* pointer;
	Ellipse test1(5, 3);
	Triangle test2(3,4,5);
	Rectangle test3(3,5);
	Rectangle test4(5);
	Polygon test5;
	test5.addPoint(0,0);
	test5.addPoint(0,4);
	test5.addPoint(2,2);
	test5.addPoint(2,0);	
	
	pointer = &test1;
	pointer->name();
	cout<<pointer->area()<<"	"<<pointer->perimeter()<<endl<<endl;
	
	pointer = &test2;
	pointer->name();
	cout<<pointer->area()<<"	"<<pointer->perimeter()<<endl<<endl;
	
	pointer = &test3;
	pointer->name();
	cout<<pointer->area()<<"	"<<pointer->perimeter()<<endl<<endl;
	
	pointer = &test4;
	pointer->name();
	cout<<pointer->area()<<"	"<<pointer->perimeter()<<endl<<endl;
	
	pointer = &test5;
	pointer->name();
	cout<<pointer->area()<<"	"<<pointer->perimeter()<<endl<<endl;
	return 0;
}
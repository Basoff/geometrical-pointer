#ifndef _ELLIPSE_H
#define _ELLIPSE_H
#include "figures.h"
class Ellipse: public Figure{
public:
	int R1, R2;
	Ellipse(int radius, int radius2);
	Ellipse(int radius);
	double area();
	double perimeter();
	void name();
};
#endif